﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FizzBuzz
{
    public static class FizBuzzClass
    {
        /// <summary>
        /// Run FizzBizz logic on a set of sequential numbers and output reults.
        /// </summary>
        /// <param name="runTo">Max number to sequentially run to.</param>
        /// <param name="DivStringList">A list of divisible numbers and associated string.</param>
        /// <param name="outWriter">Location to write to.</param>
        public static void RunFizzBuzz(int RunTo, List<FizzBuzzKeyValue> DivStringList, TextWriter outWriter)
        {

            //make sure runTo is a positive number
            if (RunTo <= 0) throw new ArgumentException(String.Format("Value entered id {0} to run to must be greater than 0", RunTo), "RunTo");

            string outputStr = "";
            bool containsDivNum = false;

            //Make a copy of the list so that the order of the original is not changed and
            //sort copy in ascending order
            List<FizzBuzzKeyValue> OrderedDivStringList = new List<FizzBuzzKeyValue>(DivStringList);
            OrderedDivStringList.Sort((s1, s2) => s1.DivisibleBy.CompareTo(s2.DivisibleBy));

            //Loop through each value up to the max
            for (int i = 1; i <= RunTo; i++)
            {
                //For each FizzBuzz divisible/string key, check to see if the current value is divisible by any of the divisible numbers. 
                //If it is divisile, append the associated string.
                foreach (FizzBuzzKeyValue KV in OrderedDivStringList)
                    if ((i % KV.DivisibleBy) == 0)
                    {
                        outputStr += KV.OutputStr + " ";
                        containsDivNum = true;
                    }

                if (containsDivNum)
                    outputStr = outputStr.TrimEnd(' ') + "\n";
                else
                    outputStr += i + "\n";
                containsDivNum = false;
            }

            outWriter.Write(outputStr);
        }
    }

    public struct FizzBuzzKeyValue
    {
        public FizzBuzzKeyValue(int divisibleNumber, string outputStr) :this()
        {
            if (divisibleNumber == 0) throw new ArgumentException("Divisible number cannot be 0.");
            this.DivisibleBy = divisibleNumber;
            this.OutputStr = outputStr;
        }
        public int DivisibleBy { get; private set; }
        public string OutputStr { get; private set; }
    }
}
