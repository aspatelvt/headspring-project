﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzBuzz;

namespace FizzBuzzApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            List<FizzBuzzKeyValue> KeyValueList = new List<FizzBuzzKeyValue>();

            bool enterMore = true;
            int divisibleValue = 1;
            string inputVal = "";
            int size = 0;

            try
            {
                Console.Write("Up to what value will FizzBuzz run to? ");
                int runto = Int32.Parse(Console.ReadLine());
                Console.WriteLine();

                //While the user enters values
                while (enterMore)
                {
                    Console.Write(String.Format("Please enter a divisible value or enter 'd' if you are done ({0}): ", size));
                    inputVal = Console.ReadLine();

                    //exit loop if user enters a value of 'd'
                    if (inputVal[0] == 'd')
                        enterMore = false;
                    else
                    {
                        divisibleValue = Int32.Parse(inputVal);
                        Console.Write(String.Format("Please enter a string value for {0}: ", divisibleValue));
                        inputVal = Console.ReadLine();
                        Console.WriteLine();

                        KeyValueList.Add(new FizzBuzzKeyValue(divisibleValue, inputVal));
                        size++;
                    }
                }

                FizBuzzClass.RunFizzBuzz(runto, KeyValueList, Console.Out);

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
    }
}
