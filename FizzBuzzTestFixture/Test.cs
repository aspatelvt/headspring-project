﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NUnit.Framework;
using FizzBuzz;

namespace FizzBuzzTestFixture
{
    [TestFixture]
    public class Test
    {
        
        //Test if zero divisible values is passed in
        [Test]
        public void ZeroDivisibleNumbersTest()
        {
            using (StringWriter sw = new StringWriter())
            {
                Console.SetOut(sw);
                List<FizzBuzzKeyValue> KeyValueList = new List<FizzBuzzKeyValue>();

                FizBuzzClass.RunFizzBuzz(10, KeyValueList, Console.Out);

                string expected = string.Format("1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n", Environment.NewLine);
                Assert.AreEqual(expected, sw.ToString());
            }
        }

        //Test if an exception is thrown for when the paramater RunTo is set to zero
        [Test]
        public void ExceptionThrownWhenRunToEqualsZeroTest()
        {
            StringWriter sw = new StringWriter();

            Console.SetOut(sw);
            List<FizzBuzzKeyValue> KeyValueList = new List<FizzBuzzKeyValue>();
            KeyValueList.Add(new FizzBuzzKeyValue(3, "fizz"));
            KeyValueList.Add(new FizzBuzzKeyValue(4, "buzz"));

            try
            {
                FizBuzzClass.RunFizzBuzz(0, KeyValueList, Console.Out);
                Assert.Fail("Expected an exception, but none was thrown");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(ex.ParamName, "RunTo");
                Assert.AreEqual(ex.Message, "Value entered id 0 to run to must be greater than 0\r\nParameter name: RunTo");
            }
        }

        //This test will run from zero to ten checking for divisible numbers 3 and 4 with strings "three, and "four". 
        //The test will contain a mix of one and no divisiable numbers for any of the 10 test values.
        [Test]
        public void NoMoreThanOneDivisibleNumbersTest()
        {
            StringWriter sw = new StringWriter();

            Console.SetOut(sw);
            List<FizzBuzzKeyValue> KeyValueList = new List<FizzBuzzKeyValue>();
            KeyValueList.Add(new FizzBuzzKeyValue(3, "three"));
            KeyValueList.Add(new FizzBuzzKeyValue(4, "four"));

            FizBuzzClass.RunFizzBuzz(10, KeyValueList, Console.Out);

            string expected = string.Format("1\n2\nthree\nfour\n5\nthree\n7\nfour\nthree\n10\n", Environment.NewLine);
            Assert.AreEqual(expected, sw.ToString());
        }

        //This test will run from zero to ten checking for divisible numbers 2, 3 and 4 with strings "two, "three, and "four". 
        //The test will contain a mix of one, two and no divisiable numbers for any of the 10 test values.
        [Test]
        public void NoMoreThanTwoDivisibleNumbersTest()
        {
            StringWriter sw = new StringWriter();
            List<FizzBuzzKeyValue> KeyValueList = new List<FizzBuzzKeyValue>();
            KeyValueList.Add(new FizzBuzzKeyValue(2, "two"));
            KeyValueList.Add(new FizzBuzzKeyValue(3, "three"));
            KeyValueList.Add(new FizzBuzzKeyValue(4, "four"));

            FizBuzzClass.RunFizzBuzz(10, KeyValueList, sw);

            string expected = string.Format("1\ntwo\nthree\ntwo four\n5\ntwo three\n7\ntwo four\nthree\ntwo\n", Environment.NewLine);
            Assert.AreEqual(expected, sw.ToString());
        }

        //This test will run from zero to ten checking for divisible numbers 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 with strings "one", "two, "three, "four". "five",
        //"six", "seven", "eight", "nine, and "ten".
        //The test will contain mathcing divisible numbers for all of the 10 test values.
        [Test]
        public void AllDivisibleNumbersTest()
        {
            StringWriter sw = new StringWriter();
            List<FizzBuzzKeyValue> KeyValueList = new List<FizzBuzzKeyValue>();
            KeyValueList.Add(new FizzBuzzKeyValue(1, "one"));
            KeyValueList.Add(new FizzBuzzKeyValue(2, "two"));
            KeyValueList.Add(new FizzBuzzKeyValue(3, "three"));
            KeyValueList.Add(new FizzBuzzKeyValue(4, "four"));
            KeyValueList.Add(new FizzBuzzKeyValue(5, "five"));
            KeyValueList.Add(new FizzBuzzKeyValue(6, "six"));
            KeyValueList.Add(new FizzBuzzKeyValue(7, "seven"));
            KeyValueList.Add(new FizzBuzzKeyValue(8, "eight"));
            KeyValueList.Add(new FizzBuzzKeyValue(9, "nine"));
            KeyValueList.Add(new FizzBuzzKeyValue(10, "ten"));

            FizBuzzClass.RunFizzBuzz(10, KeyValueList, sw);

            string expected = string.Format("one\none two\none three\none two four\none five\none two three six\none seven\none two four eight\none three nine\none two five ten\n", Environment.NewLine);
            Assert.AreEqual(expected, sw.ToString());

        }

        //This test will run from zero to ten checking for divisible numbers 3 with strings "three, and "three_2". 
        //The test will contain a mix of one and no divisiable numbers for any of the 10 test values.
        [Test]
        public void TwoDuplicateDivisibleNumbersTest()
        {
            StringWriter sw = new StringWriter();

            Console.SetOut(sw);
            List<FizzBuzzKeyValue> KeyValueList = new List<FizzBuzzKeyValue>();
            KeyValueList.Add(new FizzBuzzKeyValue(3, "three"));
            KeyValueList.Add(new FizzBuzzKeyValue(3, "three_2"));

            FizBuzzClass.RunFizzBuzz(10, KeyValueList, Console.Out);

            string expected = string.Format("1\n2\nthree three_2\n4\n5\nthree three_2\n7\n8\nthree three_2\n10\n", Environment.NewLine);
            Assert.AreEqual(expected, sw.ToString());
        }

        //This test will run from zero to ten checking for divisible numbers 4, 3, 2, -2, -3 and -4 with strings "two, "three, "four", "negtwo, "negthree, and "negfour". 
        //The test will contain a mix of one, two and no divisiable numbers for any of the 10 test values.
        //It will if values entered in non acending order will display it's token in the correct order.
        //It will test negative divisible numbers. 
        [Test]
        public void MixOrderWithNegativeDivisibleValuesTest()
        {
            StringWriter sw = new StringWriter();
            List<FizzBuzzKeyValue> KeyValueList = new List<FizzBuzzKeyValue>();
            KeyValueList.Add(new FizzBuzzKeyValue(4, "four"));
            KeyValueList.Add(new FizzBuzzKeyValue(3, "three"));
            KeyValueList.Add(new FizzBuzzKeyValue(2, "two"));
            KeyValueList.Add(new FizzBuzzKeyValue(-2, "negtwo"));
            KeyValueList.Add(new FizzBuzzKeyValue(-3, "negthree"));
            KeyValueList.Add(new FizzBuzzKeyValue(-4, "negfour"));

            FizBuzzClass.RunFizzBuzz(10, KeyValueList, sw);

            string expected = string.Format("1\nnegtwo two\nnegthree three\nnegfour negtwo two four\n5\nnegthree negtwo two three\n7\nnegfour negtwo two four\nnegthree three\nnegtwo two\n", Environment.NewLine);
            Assert.AreEqual(expected, sw.ToString());
        }
    }
}
